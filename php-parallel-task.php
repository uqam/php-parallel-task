#!/bin/php
# Sample script to be use as a "quick start" to launch parallel tasks.
# Adapt this code, logfile, args, ..
#

<?php

  function action($arg1, $arg2, $logfile) {
    exec ( "/usr/bin/java -Xmx300m -jar /path/to/file.jar --arg1 ".$arg1." --password1 ".$arg2."", $output, $return_value);
    return $return_value;
  }

  function get_data() {
    //Do whatever here, read file or database, fill array
    $return_array = array(
         "foo" => "bar",
         "bar" => "foo"
     );
    return $return_array;
  }

  function action_on_success ($arg1, $arg2, $logfile) {
  //Do whatever here if sub process exit code is 0 (OK)
  }

  function action_on_failure ($arg1, $arg2, $logfile) {
  //Do whatever here if sub process exit code is not 0 (not OK)
  }

  // PHP will fork for each rows in your array
  $array_loop = get_data();

  foreach($array_loop as $key => $value) {
    $pid = pcntl_fork();
    if ($pid == -1) {
      die('Fork failure');
    }
    else if($pid) {
      // Processus parent
      echo "New fork PID ".$pid."\n";
      $childs[] = $pid;
    }
    else
    {
      // Child - Adapt your logfile
      $logfile = fopen('/var/log/'.$value['foo'], "a+");
      $return = action($arg1, $arg2, $logfile);

      if ($return == 0) {
        action_on_success($arg1, $arg2, $logfile);
      }
      else {
        action_on_failure($arg1, $arg2, $logfile);
      }

      fclose($logfile);
      exit(0);
    }
  }

  // Wait for all child to finish before we exit main process
  while(count($childs) > 0) {
    foreach($childs as $key => $pid) {
        $res = pcntl_waitpid($pid, $status, WNOHANG);

        // If the process has already exited
        if($res == -1 || $res > 0)
            unset($childs[$key]);
    }
    sleep(1);
  }

  exit(0);
?>
